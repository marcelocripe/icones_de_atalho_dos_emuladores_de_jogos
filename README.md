pt-BR:

Contém: os ícones de atalho (arquivos ".desktop") dos emuladores de jogos para o antiX em idioma Português do Brasil, Português de Portugal/Europeu, Alemão, Francês, Italiano e Inglês (é necessário a revisão dos textos por pessoas nativas do idioma "pt" e "en") e um arquivo de texto em idioma "pt-BR" e "it" explicando como utilizá-los.

Baixe/descarregue/transfira o arquivo compactado ".zip" e o arquivo de texto ".txt".

Todos os créditos e direitos estão incluídos nos arquivos em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Enthält: Verknüpfungssymbole (".desktop"-Dateien) von Spieleemulatoren für antiX in brasilianischem Portugiesisch, portugiesischem/europäischem Portugiesisch, Deutsch, Französisch, Italienisch und Englisch (erfordert Korrekturlesen durch Muttersprachler Sprache "pt" und "en") und einen Text Datei in der Sprache "pt-BR" und "it", die erklärt, wie man sie benutzt.

Laden Sie die komprimierte „.zip“-Datei und die „.txt“-Textdatei herunter.

Alle Credits und Rechte sind in den Dateien in Bezug auf die freiwillige Arbeit jeder Person enthalten, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en-US:

Contains: shortcut icons (".desktop" files) of game emulators for antiX in Brazilian Portuguese, Portuguese/European Portuguese, German, French, Italian and English (requires proofreading by native speakers language "pt" and "en") and a text file in language "pt-BR" and "it" explaining how to use them.

Download the ".zip" compressed file and the ".txt" text file.
All credits and rights are included in the files in respect of the voluntary work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

fr :

Contient : icônes de raccourci (fichiers ".desktop") des émulateurs de jeux pour antiX en portugais brésilien, portugais/portugais européen, allemand, français, italien et anglais (nécessite une relecture par des locuteurs natifs de langue "pt" et "en") et un texte fichier en langage "pt-BR" et "it" expliquant comment les utiliser.

Téléchargez le fichier compressé ".zip" et le fichier texte ".txt".

Tous les crédits et droits sont inclus dans les fichiers en ce qui concerne le travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site Web.

marcelocripe

- - - - -

it:
Contiene: icone di scelta rapida (file ".desktop") di emulatori di gioco per antiX in portoghese brasiliano, portoghese/portoghese europeo, tedesco, francese, italiano e inglese (richiede correzione di bozze da parte di madrelingua lingua "pt" e "en") e un testo file in lingua "pt-BR" e "it" che spiega come usarli.

Scarica il file compresso ".zip" e il file di testo ".txt".

Tutti i crediti ei diritti sono inclusi nei file in relazione al lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe
